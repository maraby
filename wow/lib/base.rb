#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# BASE LIBRARY ############################

module WoW
  VERSION = '0.0.1'
end

# LIBRARY FILES ###########################

# Library Paths
BASE_PATH = File.dirname(__FILE__)
LIB_PATH = File.join(BASE_PATH, 'wow/')
TEST_PATH = File.join(BASE_PATH, 'test/')

# Dependencies
require File.join(BASE_PATH, 'dependencies')

# Library Classes
require File.join(LIB_PATH, 'character')
require File.join(LIB_PATH, 'tools')
