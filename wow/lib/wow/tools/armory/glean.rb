#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Tools
    module Armory
      class Glean
        VERSION = '0.0.1'
        
        def self.pull_profile(realm, character, euro)
          # User-Agent is set to Firefox because Armory returns the XML file instead of the XSLT transformed XML file
          if(euro)
            Hpricot(open("http://armory.wow-europe.com/character-sheet.xml?r=#{realm.gsub(/ /, '+')}&n=#{character}", "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.8.1.4) Gecko/20070515 Firefox/2.0.0.4"))
          else
            Hpricot(open("http://armory.worldofwarcraft.com/character-sheet.xml?r=#{realm.gsub(/ /, '+')}&n=#{character}", "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.8.1.4) Gecko/20070515 Firefox/2.0.0.4"))
          end
          # Hpricot(open(File.join(TEST_PATH, 'maraby.xml')))
        end
        
        def self.pull_talents(realm, character, euro)
          if(euro)
            Hpricot(open("http://armory.wow-europe.com/character-talents.xml?r=#{realm.gsub(/ /, '+')}&n=#{character}", "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.8.1.4) Gecko/20070515 Firefox/2.0.0.4")).at('talenttree')['value']
          else
            Hpricot(open("http://armory.worldofwarcraft.com/character-talents.xml?r=#{realm.gsub(/ /, '+')}&n=#{character}", "User-Agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.8.1.4) Gecko/20070515 Firefox/2.0.0.4")).at('talenttree')['value']
          end
        end
        
      end
    end
  end
end

def glean(realm, character, euro=false)
  profile = WoW::Tools::Armory::Glean.pull_profile(realm, character, euro)
  talents = WoW::Tools::Armory::Glean.pull_talents(realm, character, euro)
  WoW::Character::Base.create(profile, talents)
end

def maraby
  glean("Kil'jaeden", "Maraby")
end
