#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# RESOURCES ################################

# URL(Combat Ratings):        http://blue.cardplace.com/newcache/us/106616902.htm

# IMPLEMENTATION ###########################

module WoW
  module Stats
    
    class Base
      RATING = nil
    end
    
    # DEFENSIVE ############################
    
    class Armor < Base
      # URL: http://www.wowwiki.com/Formulas:Damage_reduction
    end
    
    class Defense < Base
      RATING = 2.4
    end
    
    class Dodge < Base
      RATING = 18.9
    end
    
    class Resilience < Base
      RATING = 39.4
    end
    
    class Block < Base
      RATING = 7.9
    end
    
    class Parry < Base
      RATING = 22.4
    end
    
    # OFFENSIVE (MELEE) ####################
    
    class Strength < Base
      #
    end
    
    class Agility < Base
      #
    end
    
    class AttackPower < Base
      #
    end
    
    class Crit < Base
      RATING = 22.1
    end
    
    class Hit < Base
      RATING = 15.8
    end
    
    class Haste < Base
      RATING = 10.5
    end
    
    # OFFENSIVE (SPELL) ####################
    
    class SpellDamage < Base
      #
    end
    
    class SpellCrit < Base
      RATING = 22.1
    end
    
    class SpellHit < Base
      RATING = 12.6
    end
    
    class SpellHaste < Base
      RATING = 21
    end
    
  end
end
