#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# RESOURCES ################################

#

# IMPLEMENTATION ###########################

module WoW
  module Gear
    
    class Base
      @stats = []
    end
    
    # QUALITY ##############################
    
    Quality = [:green, :blue, :purple, :orange]
    
    # ARMOR CLASS ##########################
    
    Armor = [:cloth, :leather, :mail, :plate]
    
    # SLOTS (ARMOR) ########################
    
    Slot = [:head, :neck, :shoulder, :back, :chest, :wrist, :hands, :waist, :legs, :feet, :finger, :trinket, :main_hand, :off_hand, :relic]
    
  end
end
