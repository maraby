#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# RESOURCES ################################

#

# IMPLEMENTATION ###########################

module WoW
  module Character
    module Classes
      
      # CLASSES ############################
      
      class Druid < Base
        @forms = [:dire_bear, :cat]
        @form = nil
        
        def dire_bear?
          # test to see if already in dire bear
          if @form == :dire_bear
            true
          else
            if @form == nil
              # test buffs for dire bear form buff and indicate if it is there or not
              if false
                false
              else
                true
              end
            else
              false
            end
          end
        end
        def dire_bear
          if !dire_bear?
            #
          else
            #
          end
          # modify the stats based on being in Dire Bear Form
        end
      end
      
      class Hunter < Base
        #
      end
      
      class Mage < Base
        #
      end
      
      class Paladin < Base
        #
      end
      
      class Priest < Base
        #
      end
      
      class Rogue < Base
        #
      end
      
      class Shaman < Base
        #
      end
      
      class Warlock < Base
        #
      end
      
      class Warrior < Base
        @stances = [:battle, :defensive, :berserker]
      end
      
    end
  end
end

# LIBRARY FILES ###########################

# Library Classes
require File.join(LIB_PATH, 'character/classes/druid')
require File.join(LIB_PATH, 'character/classes/hunter')
require File.join(LIB_PATH, 'character/classes/mage')
require File.join(LIB_PATH, 'character/classes/paladin')
require File.join(LIB_PATH, 'character/classes/priest')
require File.join(LIB_PATH, 'character/classes/rogue')
require File.join(LIB_PATH, 'character/classes/shaman')
require File.join(LIB_PATH, 'character/classes/warlock')
require File.join(LIB_PATH, 'character/classes/warrior')
