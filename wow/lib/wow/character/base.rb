#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# RESOURCES ################################

#

# IMPLEMENTATION ###########################

module WoW
  module Character
    
    class Base
      attr_reader :profile, :character, :talents, :stats
      
      def initialize(profile, talents)
        @profile = profile
        @character = {
          :name => profile.at('character')['name'],
          :level => profile.at('character')['level'].to_i,
          :class => profile.at('character')['class'],
          :race => profile.at('character')['race'],
          :faction => profile.at('character')['faction'],
          :title => profile.at('character')['title'],
          :realm => profile.at('character')['realm'],
          :guild => profile.at('character')['guildname'],
          :battlegroup => profile.at('character')['battlegroup']
        }
        # klass = profile.at('character')['class']
        @talents = WoW::Character::Talents.module_eval("#{@character[:class]}.new(talents)")
        @stats = {
          :health => profile.at('characterbars').at('health')['effective'].to_i,
          :mana => profile.at('characterbars').at('secondbar')['effective'].to_i,
          
          :base => {
            :strength => profile.at('basestats').at('strength')['effective'].to_i,
            :agility => profile.at('basestats').at('agility')['effective'].to_i,
            :stamina => profile.at('basestats').at('stamina')['effective'].to_i,
            :intellect => profile.at('basestats').at('intellect')['effective'].to_i,
            :spirit => profile.at('basestats').at('spirit')['effective'].to_i,
            :armor => profile.at('basestats').at('armor')['effective'].to_i
          },
          
          :resistances => {
            :arcane => profile.at('resistances').at('arcane')['value'].to_i,
            :fire => profile.at('resistances').at('fire')['value'].to_i,
            :frost => profile.at('resistances').at('frost')['value'].to_i,
            :holy => profile.at('resistances').at('holy')['value'].to_i,
            :nature => profile.at('resistances').at('nature')['value'].to_i,
            :shadow => profile.at('resistances').at('shadow')['value'].to_i
          },
          
          :melee => {
            :mainhandweaponskill => {
              :rating => profile.at('melee').at('mainhandweaponskill')['rating'].to_i,
              :value => profile.at('melee').at('mainhandweaponskill')['value'].to_i
            },
            :offhandweaponskill => {
              :rating => profile.at('melee').at('offhandweaponskill')['rating'].to_i,
              :value => profile.at('melee').at('offhandweaponskill')['value'].to_i
            },
            :mainhanddamage => {
              :max => profile.at('melee').at('mainhanddamage')['max'].to_i,
              :speed => profile.at('melee').at('mainhanddamage')['speed'].to_f,
              :min => profile.at('melee').at('mainhanddamage')['min'].to_i,
              :dps => profile.at('melee').at('mainhanddamage')['dps'].to_f,
              :percent => profile.at('melee').at('mainhanddamage')['percent'].to_f
            },
            :offhanddamage => {
              :max => profile.at('melee').at('offhanddamage')['max'].to_i,
              :speed => profile.at('melee').at('offhanddamage')['speed'].to_f,
              :min => profile.at('melee').at('offhanddamage')['min'].to_i,
              :dps => profile.at('melee').at('offhanddamage')['dps'].to_f,
              :percent => profile.at('melee').at('offhanddamage')['percent'].to_f
            },
            :mainhandspeed => {
              :hasterating => profile.at('melee').at('mainhandspeed')['hasterating'].to_i,
              :value => profile.at('melee').at('mainhandspeed')['value'].to_f,
              :hastepercent => profile.at('melee').at('mainhandspeed')['hastepercent']
            },
            :offhandspeed => {
              :hasterating => profile.at('melee').at('offhandspeed')['hasterating'].to_i,
              :value => profile.at('melee').at('offhandspeed')['value'].to_f,
              :hastepercent => profile.at('melee').at('offhandspeed')['hastepercent'].to_f
            },
            :power => {
              :effective => profile.at('melee').at('power')['effective'].to_i,
              :increaseddps => profile.at('melee').at('power')['increaseddps'].to_f,
              :base => profile.at('melee').at('power')['base'].to_i
            },
            :hitrating => {
              :value => profile.at('melee').at('hitrating')['value'].to_i,
              :increasedhitpercent => profile.at('melee').at('hitrating')['increasedhitpercent'].to_f
            },
            :critchance => {
              :rating => profile.at('melee').at('critchance')['rating'].to_i,
              :pluspercent => profile.at('melee').at('critchance')['pluspercent'].to_f,
              :percent => profile.at('melee').at('critchance')['percent'].to_f
            }
          },
          
          :ranged => {
            :weaponskill => {
              :rating => profile.at('ranged').at('weaponskill')['rating'].to_i,
              :value => profile.at('ranged').at('weaponskill')['value'].to_i
            },
            :damaage => {
              :max => profile.at('ranged').at('damage')['max'].to_i,
              :speed => profile.at('ranged').at('damage')['speed'].to_f,
              :min => profile.at('ranged').at('damage')['min'].to_i,
              :dps => profile.at('ranged').at('damage')['dps'].to_f,
              :percent => profile.at('ranged').at('damage')['percent'].to_f
            },
            :power => {
              :effective => profile.at('ranged').at('power')['effective'].to_i,
              :increaseddps => profile.at('ranged').at('power')['increaseddps'].to_f,
              :base => profile.at('ranged').at('power')['base'].to_i
            },
            :hitrating => {
              :value => profile.at('ranged').at('hitrating')['value'].to_i,
              :increasedhitpercent => profile.at('ranged').at('hitrating')['increasedhitpercent'].to_f
            },
            :critchance => {
              :rating => profile.at('ranged').at('critchance')['rating'].to_i,
              :pluspercent => profile.at('ranged').at('critchance')['pluspercent'].to_f,
              :percent => profile.at('ranged').at('critchance')['percent'].to_i
            }
          },
          
          :spell => {
            :bonusdamage => {
              :arcane => profile.at('charactertab >> spell').at('bonusdamage').at('arcane')['value'].to_i,
              :fire => profile.at('charactertab >> spell').at('bonusdamage').at('fire')['value'].to_i,
              :frost => profile.at('charactertab >> spell').at('bonusdamage').at('frost')['value'].to_i,
              :holy => profile.at('charactertab >> spell').at('bonusdamage').at('holy')['value'].to_i,
              :nature => profile.at('charactertab >> spell').at('bonusdamage').at('nature')['value'].to_i,
              :shadow => profile.at('charactertab >> spell').at('bonusdamage').at('shadow')['value'].to_i,
              :petbonus => {
                :fromtype => profile.at('charactertab >> spell').at('bonusdamage').at('petbonus')['fromtype'],
                :damage => profile.at('charactertab >> spell').at('bonusdamage').at('petbonus')['damage'].to_i,
                :attack => profile.at('charactertab >> spell').at('bonusdamage').at('petbonus')['attack'].to_i
              }
            },
            :bonushealing => profile.at('charactertab >> spell').at('bonushealing')['value'].to_i,
            :hitrating => {
              :value => profile.at('charactertab >> spell').at('hitrating')['value'].to_i,
              :increasedhitpercent => profile.at('charactertab >> spell').at('hitrating')['increasedhitpercent'].to_f,
              :critchance => {
                :rating => profile.at('charactertab >> spell').at('hitrating').at('critchance')['rating'].to_i,
                :arcane => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('arcane')['percent'].to_f,
                :fire => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('fire')['percent'].to_f,
                :frost => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('frost')['percent'].to_f,
                :holy => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('holy')['percent'].to_f,
                :nature => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('nature')['percent'].to_f,
                :shadow => profile.at('charactertab >> spell').at('hitrating').at('critchance').at('shadow')['percent'].to_f
              }
            },
            :penetration => profile.at('charactertab >> spell').at('penetration')['value'].to_i,
            :manaregen => {
              :casting => profile.at('charactertab >> spell').at('manaregen')['casting'].to_i,
              :notcasting => profile.at('charactertab >> spell').at('manaregen')['notcasting'].to_i
            }
          },
          
          :defenses => {
            :armor => {
              :effective => profile.at('defenses').at('armor')['effective'].to_i,
              :petbonus => profile.at('defenses').at('armor')['petbonus'].to_i,
              :percent => profile.at('defenses').at('armor')['percent'].to_f,
              :base => profile.at('defenses').at('armor')['base'].to_i
            },
            :defense => {
              :rating => profile.at('defenses').at('defense')['rating'].to_i,
              :value => profile.at('defenses').at('defense')['value'].to_f,
              :plusdefense => profile.at('defenses').at('defense')['plusdefense'].to_i,
              :decreasepercent => profile.at('defenses').at('defense')['decreasepercent'].to_f,
              :increasepercent => profile.at('defenses').at('defense')['increasepercent'].to_f
            },
            :dodge => {
              :rating => profile.at('defenses').at('dodge')['rating'].to_i,
              :increasepercent => profile.at('defenses').at('dodge')['increasepercent'].to_f,
              :percent => profile.at('defenses').at('dodge')['percent'].to_f
            },
            :parry => {
              :rating => profile.at('defenses').at('parry')['rating'].to_i,
              :increasepercent => profile.at('defenses').at('parry')['increasepercent'].to_f,
              :percent => profile.at('defenses').at('parry')['percent'].to_f
            },
            :block => {
              :rating => profile.at('defenses').at('block')['rating'].to_i,
              :increasepercent => profile.at('defenses').at('block')['increasepercent'].to_f,
              :percent => profile.at('defenses').at('block')['percent'].to_f
            },
            :resilience => {
              :damagepercent => profile.at('defenses').at('block')['damagepercent'].to_f,
              :value => profile.at('defenses').at('block')['value'].to_f,
              :hitpercent => profile.at('defenses').at('block')['hitpercent'].to_f
            }
          }
        }
      end
      
      def method_missing(name, *args)
        return @character[name] unless @character[name].nil?
        return @stats[name] unless @stats[name].nil?
        return @profile.at(name)
      end
      
      def inspect
        "#<#{self.class} #{@character[:name]}(Lvl#{@character[:level]})>"
      end
      
      def self.create(profile, talents)
        klass = profile.at('character')['class']
        WoW::Character::Classes.module_eval("#{klass}.new(profile, talents)")
      end
    end
    
  end
end
