#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Paladin < Base
        def set_names
          @trees[:first][:name] = 'Holy'
          @trees[:second][:name] = 'Protection'
          @trees[:third][:name] = 'Retribution'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
