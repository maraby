#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Mage < Base
        def set_names
          @trees[:first][:name] = 'Arcane'
          @trees[:second][:name] = 'Fire'
          @trees[:third][:name] = 'Frost'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
