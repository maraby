#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Warrior < Base
        def set_names
          @trees[:first][:name] = 'Arms'
          @trees[:second][:name] = 'Fury'
          @trees[:third][:name] = 'Protection'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
