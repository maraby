#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Priest < Base
        def set_names
          @trees[:first][:name] = 'Descipline'
          @trees[:second][:name] = 'Holy'
          @trees[:third][:name] = 'Shadow'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
