#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Druid < Base
        def set_names
          @trees[:first][:name] = 'Balance'
          @trees[:second][:name] = 'Feral'
          @trees[:third][:name] = 'Restoration'
        end
        
        def set_talents
          
          # FERAL TREE #####################
          
          # tier 1
          @talents[21] = Talent.new('Ferocity', 'Feral', 'Reduces the cost of your Maul, Swipe, Claw, Rake and Mangle abilities by %n rage or energy.', [[1, 2, 3, 4, 5]]) {|stats, ranks, rank| stats}
          @talents[22] = Talent.new('Feral Aggression', 'Feral', 'Increases the Attack Power reduction of your Demoralizing Roar by %n%% and the damage caused by your Ferocious Bite by %n%%.', [[8, 16, 24, 32, 40],[3,6,9,12,15]]) {|stats, ranks, rank| stats}
          
          # tier 2
          @talents[23] = Talent.new('Feral Instinct', 'Feral', 'Increases threat caused in Bear and Dire Bear Form by %n%% and reduces the chance enemies have to detect you while Prowling.', [[5, 10, 15]]) {|stats, ranks, rank| stats}
          @talents[24] = Talent.new('Brutal Impact', 'Feral', 'Increases the stun duration of your Bash and Pounce abilities by %n sec.', [[0.5, 1]]) {|stats, ranks, rank| stats}
          @talents[25] = Talent.new('Thick Hide', 'Feral', 'Increases your Armor contribution from items by %n%%.', [[1.04, 1.07, 1.10]]) do |stats, ranks, rank|
            armor = stats[:defenses][:armor][:effective]
            thick_armor = armor * ranks[rank-1]
            stats[:base][:armor] = thick_armor
            stats[:defenses][:armor][:effective] = thick_armor
            stats
          end
          
          # tier 3
          @talents[26] = Talent.new('Feral Swiftness', 'Feral', 'Increases your movement speed by %n%% while outdoors in Cat Form and increases your chance to dodge while in Cat Form, Bear Form and Dire Bear form by %n%%.', [[15,30],[2.0, 4.0]]) do |stats, ranks, rank|
            ranks = ranks[1]
            dodge = stats[:defenses][:dodge][:percent]
            swift_dodge = dodge + ranks[rank-1]
            stats[:defenses][:dodge][:percent] = swift_dodge
            stats
          end
          @talents[27] = Talent.new('Feral Charge', 'Feral', 'Teaches the ability Feral Charge.', []) {|stats, ranks, rank| stats}
          @talents[28] = Talent.new('Sharpened Claws', 'Feral', 'Increases your critical strike chance while in Bear, Dire Bear or Cat Form by %n%%.', [[2.0, 4.0, 6.0]]) do |stats, ranks, rank|
            critical = stats[:melee][:critchance][:percent]
            sharp_crit = critical + ranks[rank-1]
            stats[:melee][:critchance][:percent] = sharp_crit
            stats
          end
          
          # tier 4
          @talents[29] = Talent.new('Shredding Attacks', 'Feral', 'Reduces the energy cost of your Shred ability by %n and the rage cost of your Lacerate ability by %n.', [[9, 18], [1, 2]]) {|stats, ranks, rank| stats}
          @talents[30] = Talent.new('Predetory Strikes', 'Feral', 'Increases your melee attack power in Cat, Bear, Dire Bear and Moonkin Forms by %n%% of your level.', [[0.50, 1.00, 1.50]]) do |stats, ranks, rank|
            power = stats[:melee][:power][:effective]
            predatory_power = power * ranks[rank-1]
            stats[:melee][:power][:effective] = predatory_power
            stats
          end
          @talents[31] = Talent.new('Primal Fury', 'Feral', 'Gives you a %n%% chance to gain an additional 5 Rage anytime you get a critical strike while in Bear and Dire Bear Form and your critical strikes from Cat Form abilities that add combo points have a %n%% chance to add an additional combo point.', [[50, 100], [50, 100]]) {|stats, ranks, rank| stats}
          
          # tier 5
          @talents[32] = Talent.new('Savage Fury', 'Feral', 'Increases the damage caused by your Claw, Rake and Mangle (Cat) abilities by %n%%.', [[10, 20]]) {|stats, ranks, rank| stats}
          @talents[33] = Talent.new('Faeri Fire (Feral)', 'Feral', 'Teaches Faerie Fire (Feral).', []) {|stats, ranks, rank| stats}
          @talents[34] = Talent.new('Nurturing Instinct', 'Feral', 'Increases your healing spells by up to %n%% of your Strength.', [[25, 50]]) {|stats, ranks, rank| stats}
          
          # tier 6
          @talents[35] = Talent.new('Heart of the Wild', 'Feral', 'Increases your Intellect by %n%%. In addition, while in Bear or Dire Bear Form your Stamina is increased by %n%% and while in Cat Form your Strength is increased by %n%%.', [[1.04, 1.08, 1.12, 1.16, 1.20],[1.04, 1.08, 1.12, 1.16, 1.20],[1.04, 1.08, 1.12, 1.16, 1.20]]) do |stats, ranks, rank|
            strength = stats[:base][:strength]
            wild_strength = strength * ranks[1][rank-1]
            stats[:base][:strength] = wild_strength
            
            stamina = stats[:base][:stamina]
            wild_stamina = stamina * ranks[2][rank-1]
            stats[:base][:stamina] = wild_stamina
            
            stats
          end
          @talents[36] = Talent.new('Survival of the Fittest', 'Feral', 'Increases all attributes by %n%% and reduces the chance you\'ll be be critically hit by melee attacks by %n%%.', [[1,2,3],[1,2,3]]) {|stats, ranks, rank| stats}
          
          # tier 7
          @talents[37] = Talent.new('Primal Tenacity', 'Feral', 'Increases your chance to resist Stun and Fear mechanics by %n%%.', [[5, 10, 15]]) {|stats, ranks, rank| stats}
          @talents[38] = Talent.new('Leader of the Pack', 'Feral', 'While in Cat, Bear or Dire Bear Form, the Leader of the Pack increases ranged and melee critical chance of all party members within 45 years by %n%%.', [[5.0]]) do |stats, ranks, rank|
            critical = stats[:melee][:critchance][:percent]
            leading_crit = critical + ranks[rank-1]
            stats[:melee][:critchance][:percent] = leading_crit
            stats
          end
          @talents[39] = Talent.new('Improved Leader of the Pack', 'Feral', 'Your Leader of the Pack ability also causes affected targets to have a %n%% chance to heal themselves for %n%% of their total health when they critically hit with a melee or ranged attack. The healing effect cannot occur more than once every 6 sec.', [[50, 100],[2,4]]) {|stats, ranks, rank| stats}
          
          # tier 8
          @talents[40] = Talent.new('Predatory Instincts', 'Feral', 'While in Cat Form, Bear Form, or Dire Bear Form, increases your damage from melee critical strikes by %n%% and your chance to avoid area of affect attacks by %n%%.', [[2, 4, 6, 8, 10],[3,6,9,12,15]]) {|stats, ranks, rank| stats}
          
          # tier 9
          @talents[41] = Talent.new('Mangle', 'Feral', 'Teaches Mangle (Cat) and Mangle (Bear).', []) {|stats, ranks, rank| stats}
          
          # RESTORATION TREE ###############
          
          # tier 1
          @talents[42] = Talent.new('Improved Mark of the Wild', 'Restoration', 'Increases the effects of your Mark of the Wild and Gift of the Wild spells by %n%%.', [[7,14,21,28,35]]) {|stats, ranks, rank| stats}
          @talents[43] = Talent.new('Furor', 'Restoration', 'Gives you %n%% chance to gain 10 Rage when you shapeshift into Bear and Dire Bear Form or 40 Energy when you shapeshift into Cat Form.', [[20, 40, 60, 80, 100]]) {|stats, ranks, rank| stats}
          
          # tier 2
          @talents[44] = Talent.new('Naturalist', 'Restoration', 'Reduces the cast time of your Healing Touch spell by %n sec and increases the damage you deal with physical attacks in all forms by %n%%.', [[0.1, 0.2, 0.3, 0.4, 0.5],[2,4,6,8,10]]) {|stats, ranks, rank| stats}
          @talents[45] = Talent.new('Nature\'s Focus', 'Restoration', 'Gives you a %n%% chance to avoid interruption caused by damage while casting the Healing Touch, Regrowth and Tranquillity spells.', [[14,28,42,56,70]]) {|stats, ranks, rank| stats}
          @talents[46] = Talent.new('Natural Shapeshifter', 'Restoration', 'Reduces the mana cost of all shapeshifting by %n%%.', [[10,20,30]]) {|stats, ranks, rank| stats}
          
          # tier 3
          @talents[47] = Talent.new('Intensity', 'Restoration', 'Allows %n%% of your Mana regeneration to continue while casting and causes your Enrage ability to instantly generate %n rage.', [[5,10,15],[4,8,12]]) {|stats, ranks, rank| stats}
          @talents[48] = Talent.new('Subtlety', 'Restoration', 'Reduces the threat generated by your spells by %n%% and reduces the chance your spells will be dispelled by %n%%.', [[4,8,12,16,20],[6,12,18,24,30]]) {|stats, ranks, rank| stats}
          @talents[49] = Talent.new('Omen of Clarity', 'Restoration', 'Teaches Omen of Clarity.', []) {|stats, ranks, rank| stats}
          
          # tier 4
        end
      end
    end
  end
end
