#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Shaman < Base
        def set_names
          @trees[:first][:name] = 'Elemental'
          @trees[:second][:name] = 'Enhancement'
          @trees[:third][:name] = 'Restoration'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
