#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-06.
#  Copyright (c) 2007. All rights reserved.

module WoW
  module Character
    module Talents
      class Rogue < Base
        def set_names
          @trees[:first][:name] = 'Assassination'
          @trees[:second][:name] = 'Combat'
          @trees[:third][:name] = 'Subtlety'
        end
        
        def set_talents
          #
        end
      end
    end
  end
end
