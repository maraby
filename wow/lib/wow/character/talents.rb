#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# RESOURCES ################################

#

# IMPLEMENTATION ###########################

module WoW
  module Character
    module Talents
      
      class Base
        attr_reader :spec, :talents, :trees
        
        def initialize(spec)
          @talents = (0..62).to_a.collect {|x| nil}
          @spec = {
            :all => '00000000000000000000000000000000000000000000000000000000000000',
            :first => '000000000000000000000',
            :second => '000000000000000000000',
            :third => '000000000000000000000'
          }
          @trees = {
            :first => {:name => 'Undefined', :spent => 0},
            :second => {:name => 'Undefined', :spent => 0},
            :third => {:name => 'Undefined', :spent => 0}
          }
          
          # data
          @spec[:all] = spec
          @spec[:first] = @spec[:all][0..20]
          @spec[:second] = @spec[:all][21..41]
          @spec[:third] = @spec[:all][42..62]
          @trees[:first][:spent] = @spec[:first].split('').inject {|x,y| x.to_i + y.to_i}
          @trees[:second][:spent] = @spec[:second].split('').inject {|x,y| x.to_i + y.to_i}
          @trees[:third][:spent] = @spec[:third].split('').inject {|x,y| x.to_i + y.to_i}
          
          set_names
          set_talents
        end
        
        def inspect
          "#<#{self.class}:#{spec_label}(#{@trees[:first][:spent]}/#{@trees[:second][:spent]}/#{@trees[:third][:spent]})>"
        end
        
        def spec_label
          first = @trees[:first][:spent]
          second = @trees[:second][:spent]
          third = @trees[:third][:spent]
          
          if (first > 30 and second > 30) or (first > 30 and third > 30) or (second > 30 and third > 30)
            return 'Hybrid'
          end
          if first > second and first > third
            return @trees[:first][:name]
          end
          if second > first and second > third
            return @trees[:second][:name]
          end
          if third > first and third > second
            return @trees[:third][:name]
          end
        end
      end
      
      # TALENT #############################
      
      class Talent
        attr_accessor :name, :tree, :description, :rank, :effect
        def initialize(name, tree, description, rank, &effect)
          @name = name
          @tree = tree
          @description = description
          @rank = rank
          @effect = effect
        end
        def inspect
          "#<#{self.class}:#{@name}(#{@tree})>"
        end
      end
      
    end
  end
end

# LIBRARY FILES ###########################

# Library Classes
require File.join(LIB_PATH, 'character/talents/druid')
require File.join(LIB_PATH, 'character/talents/hunter')
require File.join(LIB_PATH, 'character/talents/mage')
require File.join(LIB_PATH, 'character/talents/paladin')
require File.join(LIB_PATH, 'character/talents/priest')
require File.join(LIB_PATH, 'character/talents/rogue')
require File.join(LIB_PATH, 'character/talents/shaman')
require File.join(LIB_PATH, 'character/talents/warlock')
require File.join(LIB_PATH, 'character/talents/warrior')
