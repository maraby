#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-08-05.
#  Copyright (c) 2007. All rights reserved.

# BASE LIBRARY ############################

module WoW
  module Tools
    VERSION = '0.0.1'
  end
end

# LIBRARY FILES ###########################

# Library Classes
require File.join(LIB_PATH, 'tools/armory')
