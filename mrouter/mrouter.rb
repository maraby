#--
#  Created by Matt Todd on 2008-01-02.
#  Copyright (c) 2007. All rights reserved.
#++

#--
# dependencies
#++

begin
  %w(rubygems merb/core_ext merb/router).each {|dep|require dep}
rescue LoadError => e
  abort "Merb must be installed for Routing to function. Please install Merb."
end

#--
# module
#++

module Rack
  
  # = MRouter, the Rack Merb Router
  # 
  # == Usage
  # 
  #   class RackApp
  #     include MRouterHelper
  #     route do |r|
  #       r.match('/path/to/match').to(:action => 'do_stuff')
  #       {:action => 'not_found'} # the default route
  #     end
  #     def do_stuff(params)
  #       [200, {}, 'OK']
  #     end
  #     def call(env)
  #       [200, {'Content-Type'=>'text/plain'}, env['merb.route'].inspect]
  #     end
  #   end
  # 
  # == Default Routes
  # 
  # Supplying a default route if none of the others match is good practice,
  # but is unnecessary as the predefined route is always, automatically,
  # going to contain a redirection to the +not_found+ method. (So your app
  # should implement a +not_found+ method.)
  # 
  # In order to set a different default route, simply end the call to +route+
  # with a hash containing the action to run along with any other params.
  # 
  # If your particular app requires another field to default to, such as a
  # controller of some sort, defining a default route will be necessary as
  # MRouter only knows it should default to +not_found+.
  # 
  # == The Hard Work
  # 
  # The mechanics of the router are solely from the efforts of the Merb
  # community. This functionality is completely ripped right out of Merb
  # and makes it functional. All credit to them, and be sure to check out
  # their great framework: if you need a beefier framework, maybe Merb is
  # right for you.
  # 
  # http://merbivore.com/
  class MRouter < Merb::Router
    
    def initialize(app)
      @app = app
    end
    
    def call(env)
      env['merb.router'] = self
      env['merb.route'] = self.class.route(env)
      @app.call(env)
    end
    
    # Retrieves the last value from the +route+ call and, if it's a Hash, sets
    # it to +@@default_route+ to designate the failover route. If +route+ is
    # not a Hash, though, the internal default should be used instead (as the
    # last returned value is probably a Route object returned by the
    # <tt>r.match().to()</tt> call).
    # 
    # Used exclusively internally.
    def self.default_to route
      @@default_route = route.is_a?(Hash) ? route : {:action => 'not_found'}
    end
    
    # Called internally by the +call+ method to match the current request
    # against the currently defined routes. Returns the params list defined in
    # the +to+ routing definition, opting for the default route if no match is
    # made.
    def self.route(env)
      # pull out the path requested (WEBrick keeps the host and port and protocol in REQUEST_URI)
      uri = URI.parse(env['REQUEST_URI']).path
      
      # prepare request
      path = (uri ? uri.split('?').first : '').sub(/\/+/, '/')
      path = path[0..-2] if (path[-1] == ?/) && path.size > 1
      req = Struct.new(:path, :method).new(path, env['REQUEST_METHOD'].downcase.to_sym)
      
      # perform match
      route = self.match(req, {})
      
      # make sure a route is returned even if no match is found
      if route[0].nil?
        #return default route
        @@default_route
      else
        # params (including action and module if set) for the matching route
        route[1]
      end
    end
    
  end
end

# The Helper that provides the +route+ class method and +route+ instance method
# to setup routes and to actually route the request.
module MRouterHelper
  
  # Matches the request information in +env+ to the appropriate route.
  def route
    Rack::MRouter.route(@env)
  end
  
  module ClassMethods
    # Yields the router which allows routes to be set up.
    def route
      if block_given?
        Rack::MRouter.prepare do |router|
          Rack::MRouter.default_to yield(router)
        end
      else
        abort "Halcyon::Server::Base.route expects a block to define routes."
      end
    end
  end
  
  # Hook to include the class methods into the receiver.
  def self.included(receiver)
    receiver.extend(ClassMethods)
  end
  
end
