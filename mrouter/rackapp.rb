#!/usr/bin/env ruby -wKU

require 'rubygems'
require 'rack'
require 'mrouter'

class Rap
  include MRouterHelper
  
  route do |r|
    r.match('/').to(:action => 'foo', :random => 'arbitrary')
    r.match('/hello/:name').to(:action => 'greeter')
  end
  
  def call(env)
    @env = env
    @route = env['merb.route'].dup
    send(@route.delete(:action).to_sym, @route)
  end
  
  def foo(params)
    [200, {'Content-Type'=>'text/plain'}, @route.inspect]
  end
  
  def greeter(params)
    [200, {'Content-Type'=>'text/plain'}, "Hello #{params[:name]}"]
  end
  
  def not_found(params)
    [404, {'Content-Type'=>'text/plain'}, 'Not Found']
  end
  
end

Rack::Handler::Mongrel.run Rack::MRouter.new(Rap.new), :Port => 4400 if $0 == __FILE__
