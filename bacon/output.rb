# Colorising Bacon's output.

module OutputColorizer
  # colorize
  def color(text, color_val)
    "#{color_val}#{text}\e[0m"
  end
  # colors
  def green(text); color(text, "\e[32m"); end
  def red(text); color(text, "\e[31m"); end
  def magenta(text); color(text, "\e[35m"); end
  def yellow(text); color(text, "\e[33m"); end
  def blue(text); color(text, "\e[34m"); end
  # colors but default to black on false
  def green!(text,test); return green(text) if test; text; end
  def red!(text,test); return red(text) if test; text; end
  def magenta!(text,test); return magenta(text) if test; text; end
  def yellow!(text,test); return yellow(text) if test; text; end
  def blue!(text,test); return blue(text) if test; text; end
end

module Bacon
  
  # Colorizes the SpecDox output
  module CSpecDoxOutput
    include OutputColorizer
    def handle_specification(name)
      puts name
      yield
      puts
    end
    def handle_requirement(description)
      error = yield
      if error.empty?
        puts green("- #{description}")
      else
        case label = error[0..0]
        when 'F'
          puts red("- #{description} [#{error}]")
        when 'E'
          puts magenta("- #{description} [#{error}]")
        end
      end
    end
    def handle_summary
      specs, reqs, failed, errors = Counter.values_at(:specifications, :requirements, :failed, :errors)
      puts "", ErrorLog
      msg = green("%d tests, %d assertions, %d failures, %d errors") unless failed > 0 || errors > 0
      msg = "%d tests, %d assertions, #{red!("%d failures",failed>0)}, #{magenta!("%d errors",errors>0)}"
      puts msg % Counter.values_at(:specifications, :requirements, :failed, :errors)
    end
  end
  
  # Colorizes the TestUnit output
  module CTestUnitOutput
    include OutputColorizer
    def handle_specification(name)  yield  end
    def handle_requirement(description)
      error = yield
      if error.empty?
        print green(".")
      else
        case label = error[0..0]
        when 'F'
          print red(label)
        when 'E'
          print magenta(label)
        end
      end
    end
    def handle_summary
      specs, reqs, failed, errors = Counter.values_at(:specifications, :requirements, :failed, :errors)
      puts "", ErrorLog
      msg = green("%d tests, %d assertions, %d failures, %d errors") unless failed > 0 || errors > 0
      msg = "%d tests, %d assertions, #{red!("%d failures",failed>0)}, #{magenta!("%d errors",errors>0)}"
      puts msg % Counter.values_at(:specifications, :requirements, :failed, :errors)
    end
  end
  
end
