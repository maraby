$:.unshift(File.dirname(__FILE__))
# $:.unshift(File.join(File.dirname(__FILE__), 'simple'))

module Simple
  def self.version
    "0.1.0"
  end
  
  autoload :Logger, 'simple/logger'
  autoload :Server, 'simple/server'
end
