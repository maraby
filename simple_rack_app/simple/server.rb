#!/usr/bin/env ruby

module Simple
  class Server
    def initialize(options={})
      @options = options
      @logger = options[:logger]
    end
    def call(env)
      @env = env
      @logger.info @env['REQUEST_URI']
      status, headers, body = run(@env['REQUEST_URI'])
      headers = {
        'Content-Type' => 'text/html',
        'Content-Length' => body.join.size.to_s
      }.merge(headers)
      [status, headers, body]
    end
  end
end
