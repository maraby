require 'logger'

module Simple
  class Logger < ::Logger
    def initialize(*args)
      super
      self.formatter = proc{|s,t,p,m|"#{s} [#{t.strftime("%Y-%m-%d %H:%M:%S")}] (#{$$}) #{p} :: #{m}\n"}
    end
  end
end
