/*	
 *	= Javascripts
 *	
 *	All application-specific Javascripts including objects and methods.
 *	
 */

// DynLoader
function dynload_tt(node, frag) {
	new Ajax.Request('file:///Users/mtodd/Sites/maraby/dacelo/'+frag+'.html', {
		method: 'get',
		onSuccess: function(transport) {
			$('dynload').appendChild(new Element('div').update(transport.responseText));
			new Tooltip(node, $(frag));
		}
	});
}
function dynload_tt_omo(e) {
	node = Event.element(e).parentNode;
	frag = $w(node.className).reject(function(klass){return klass == 'type'})[0];
	new Ajax.Request('file:///Users/mtodd/Sites/maraby/dacelo/'+frag+'.html', {
		method: 'get',
		onSuccess: function(transport) {
			$('dynload').appendChild(new Element('div').update(transport.responseText));
			Event.stopObserving(node, 'mouseover', dynload_tt_omo);
			tt = new Tooltip(node, $(frag));
			tt.showTooltip(e);
		}
	});
}

// Tabs
var tabView = new YAHOO.widget.TabView('tabs');

// DataTables
YAHOO.util.Event.addListener(window, "load", function() {
  YAHOO.example.EnhanceFromMarkup = new function() {
    var myColumnDefs = [
      {key:"hub_id", label:"Hub ID", formatter:YAHOO.widget.DataTable.formatNumber, sortable:true},
      {key:"service_tag", label:"Service Tag", sortable:true},
      {key:"asset_type", label:"Asset Type", sortable:true},
      {key:"state", label:"State", sortable:true},
      {key:"location", label:"Location", sortable:true},
      {key:"actions", label:"", sortable:false}
    ];
    this.myDataSource = new YAHOO.util.DataSource(YAHOO.util.Dom.get("assets"));
    this.myDataSource.responseType = YAHOO.util.DataSource.TYPE_HTMLTABLE;
    this.myDataSource.responseSchema = {
      fields: [{key:"hub_id"},
        {key:"service_tag"},
        {key:"asset_type"},
        {key:"state"},
        {key:"location"},
        {key:"actions"}
      ]
    };
    this.myDataTable = new YAHOO.widget.DataTable("assets-view", myColumnDefs, this.myDataSource, {sortedBy:{key:"service_tag",dir:"desc"}});
  };
});

// Tooltips
Event.observe(window,"load",function() {
	$$("span.location").each(function(node){
		location_tt = $w(node.className).reject(function(klass){return klass == 'location'})[0];
		new Tooltip(node, $(location_tt));
	});
	$$("span.client").each(function(node){
		client_tt = $w(node.className).reject(function(klass){return klass == 'client'})[0];
		new Tooltip(node, $(client_tt));
	});
	// $$("span.type").each(function(node){
	// 	// get the type name
	// 	type_tt = $w(node.className).reject(function(klass){return klass == 'type'})[0];
	// 	dynload_tt(node, type_tt);
	// });
	$$("span.type").each(function(node){
		node.observe('mouseover', dynload_tt_omo);
		// $('dynload').appendChild(new Element('div', {id:"dell-d820"});
	});
});
